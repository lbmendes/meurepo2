FROM alpine
RUN apk --no-cache add apache2 php8-apache2 tzdata
EXPOSE 80
ENV TZ="America/Bahia"
COPY ./index.html ./bemvindo.php /var/www/localhost/htdocs/
ENTRYPOINT ["httpd", "-D", "FOREGROUND"]

