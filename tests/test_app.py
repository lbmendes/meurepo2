"""Módulo de testes da aplicação usando requisições http."""
from urllib import request
from pytest import mark


def test_response_code_is_200():
    """Envia requisição http para a aplicação e checa se cód. retorno é 200."""
    with request.urlopen('http://localhost/') as response:
        response_code = response.code
    assert response_code == 200


@mark.parametrize(
    'feature',
    ['login', 'cadastro', 'consulta']
)
def test_features_are_working(feature):
    """Verifica se funcionalidade indicada está OK."""
    with request.urlopen('http://localhost/') as response:
        html = response.read()
    assert f'{feature} ok' in str(html)

